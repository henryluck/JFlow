package BP.WF.Template;

/** 
 延续子流程属性
 
*/
public class NodeYGFlowAttr extends BP.En.EntityOIDNameAttr
{

		///#region 基本属性
	/** 
	 标题
	 
	*/
	public static final String FK_Flow = "FK_Flow";
	/** 
	 顺序号
	 
	*/
	public static final String Idx = "Idx";
	/** 
	 显示在那里？
	 
	*/
	public static final String YGWorkWay = "YGWorkWay";
	/** 
	 节点ID
	 
	*/
	public static final String FK_Node = "FK_Node";
	//表达式类型
	public static final String ExpType = "ExpType";
	//条件表达式
	public static final String CondExp = "CondExp";
	public static final String YBFlowReturnRole = "YBFlowReturnRole";
	
	public static final String ReturnToNode = "ReturnToNode";	  
	
	


		///#endregion
}